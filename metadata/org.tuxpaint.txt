Categories:Graphics,Science & Education
License:GPLv2+
Web Site:http://tuxpaint.org
Source Code:https://github.com/perepujal/Tuxpaint-Android
Issue Tracker:https://sourceforge.net/p/tuxpaint/bugs/?source=navbar
Donate:http://www.newbreedsoftware.com/donate/

Auto Name:TuxPaint
Summary:Drawing program for children
Description:
Tux Paint is a free, award-winning drawing program for children ages 3 to 12. It
combines an easy-to-use interface, fun sound effects, and an encouraging cartoon
mascot who guides children as they use the program.

Status:Beta.
.

Repo Type:git
Repo:https://github.com/perepujal/Tuxpaint-Android.git

Build:0.9.23-beta,1
    commit=966b358a6379e272466e7dc821f2bc469e301de5
    extlibs=android/android-support-v4.jar
    scandelete=jni/
    buildjni=yes

Build:0.9.23-beta,2
    commit=4b5b368bfe2c471381197f275ccf9349b2b9bda2
    extlibs=android/android-support-v4.jar
    scandelete=jni/
    buildjni=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.9.23-beta
Current Version Code:2

